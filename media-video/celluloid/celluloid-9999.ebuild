# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit gnome2-utils meson xdg

DESCRIPTION="A simple GTK+ frontend for mpv"
HOMEPAGE="https://celluloid-player.github.io/"

if [[ ${PV} == 9999 ]]
then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/celluloid-player/${PN}"
else
	SRC_URI="https://github.com/celluloid-player/${PN}/releases/download/${P}.tar.xz"
	S="${WORKDIR}/${P}"
	KEYWORDS="~amd64"
fi

LICENSE="GPL-3+"
SLOT="0"

DEPEND="
	>=dev-libs/glib-2.66.0:2
	>=gui-libs/gtk-4.6.1:4
	>=gui-libs/libadwaita-1.2.0:1
	media-libs/libepoxy
	>=media-video/mpv-0.32:=[libmpv]
"
RDEPEND="${DEPEND}"
BDEPEND="
	dev-libs/appstream-glib
	virtual/pkgconfig
"

pkg_postinst() {
	xdg_pkg_postinst
	gnome2_schemas_update
}

pkg_postrm() {
	xdg_pkg_postrm
	gnome2_schemas_update
}
